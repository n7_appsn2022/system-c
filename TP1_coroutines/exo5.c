/* Jeu stupide.
 * 4 joueurs: bleu, vert, jaune, rouge.
 * Quand bleu à la main, il la donne à vert, jaune ou rouge (aléatoirement).
 * Quand vert à la main, il la donne à bleu ou rouge.
 * Quand rouge à la main, il la donne à vert ou bleu.
 * Quand jaune à la main, il la donne à vert ou retourne au programme principal qui termine.
 */
   
#include "coroutines.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#pragma GCC diagnostic ignored "-Wunused-parameter"

coroutine_t c0, bleu, jaune, rouge, vert;

/* renvoie une valeur entre 1 et max (inclus) */
int jeter_des(int max) {
    return 1 + (random() % max);
}

void joueur_bleu (void *unused)
{
    while (1) {
        printf("bleu\n");
        int des = jeter_des(3);
        switch (des){
            case 1:{
                cor_transferer(bleu,rouge);
                break;
            }
            case 2:{
                cor_transferer(bleu,vert);
                break;
            }
            case 3: {
                cor_transferer(bleu,jaune);
                break;
            }
            default: {
                break;
            }
        }
        /* ... */
    }
}

void joueur_vert (void *unused)
{
    while (1) {
        printf("vert\n");
        int des = jeter_des(2);
        /* ... */
        switch (des){
            case 1:{
                cor_transferer(vert,rouge);
                break;
            }
            case 2:{
                cor_transferer(vert,bleu);
                break;
            }
            default: {
                break;
            }
        }
    }
}

void joueur_rouge (void *unused)
{
    while (1) {
        printf("rouge\n");
        int des = jeter_des(2);
        /* ... */
        switch (des){
            case 1:{
                cor_transferer(rouge,bleu);
                break;
            }
            case 2:{
                cor_transferer(rouge,vert);
                break;
            }
            default: {
                break;
            }
        }
    }
}

void joueur_jaune (void *unused)
{
    while (1) {
        printf("jaune\n");
        int des = jeter_des(2);
        /* ... */
        switch (des){
            case 1:{
                cor_transferer(jaune,c0);
                break;
            }
            case 2:{
                cor_transferer(jaune,vert);
                break;
            }
            default: {
                break;
            }
        }
    }
}

int main()
{
    srandom(getpid());
    /* ... */
    printf ("Debut du jeu\n");
    c0 = cor_creer("main",NULL,NULL);
    rouge = cor_creer("rouge",joueur_rouge,NULL);
    jaune = cor_creer("jaune",joueur_jaune,NULL);
    vert = cor_creer("vert",joueur_vert,NULL);
    bleu = cor_creer("bleu",joueur_bleu,NULL);
    cor_transferer(c0,bleu);
    /* ... */
    printf ("Fin du jeu\n");
    return 0;
}
