
/* ping/pong coroutines. */
   
#include "coroutines.h"
#include <stdio.h>
#include <stdlib.h>

#define N 10
coroutine_t c1,c2;
void ping_or_pong (void *texte)
{
    
    int i;
    for (i = 0; i < N; i++) {
        printf ("%s\n", (char *)texte);
        cor_transferer(c1,c1);
    }
    cor_transferer(c2,c2);
}

int main()
{
    printf ("Debut\n");
    c1 = cor_creer("pongeur",ping_or_pong,"pong");
    c2 = cor_creer("pingeur",ping_or_pong,"ping");
    cor_transferer(c2,c2);
    printf ("Fin\n");
    return 0;
}
