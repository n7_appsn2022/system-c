
/* ping/pong coroutines. */
   
#include "coroutines.h"
#include <stdio.h>
#include <stdlib.h>
#pragma GCC diagnostic ignored "-Wunused-parameter"

#define N 10
coroutine_t pingeur,active;

void pong (void *unused)
{
    int i;
    for (i = 0; i < N; i++) {
        printf ("     pong\n");
        //Passer la main a ping
        cor_transferer(active,active);
    }
    printf ("Pong a fini\n");
    // rend la main au main pour une terminaison propre
    cor_transferer(active,pingeur);

}

void ping (void *unused)
{
    int i;
    for (i = 0; i < N; i++) {
        printf ("ping\n");
        //passe la main a pong
        cor_transferer(active,active);
    }
    printf ("Ping a fini\n");
    //passe la main a pong
    cor_transferer(active,active);
}

int main()
{
    
    printf ("Debut\n");
    pingeur = cor_creer("pingeur", ping, NULL);
    active = cor_creer("pongeur", pong, NULL);
    cor_transferer(pingeur,pingeur);
    printf ("Fin\n");
    return 0;
}
