
/* Doit afficher: "1 2 3 4 5"
 *  et terminer par "c'est fini."
 */
#include "coroutines.h"
#include <stdio.h>
#include <stdlib.h>
#pragma GCC diagnostic ignored "-Wunused-parameter"

coroutine_t c0,c1,c2,c3;

void code1 (void *unused)
{
    printf("1\n");
    cor_transferer(c1,c2);
    printf("3\n");
    cor_transferer(c1,c3);
}

void code2 (void *unused)
{
    printf("2\n");
    cor_transferer(c2,c1);
    printf("5\n");
    cor_transferer(c2,c0);
}

void code3 (void *unused)
{
    printf("4\n");
    cor_transferer(c3,c2);
}

int main()
{
    c0 = cor_creer("c0",NULL,"unused");
    c1 = cor_creer("c1",code1,"unused");
    c2 = cor_creer("c2",code2,"unused");
    c3 = cor_creer("c3",code3,"unused");
    cor_transferer(c0,c1);
    printf ("C'est fini.\n");
    return 0;
}
