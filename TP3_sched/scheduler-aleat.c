/* Time-stamp: <29 jui 2010 09:16 queinnec@enseeiht.fr> */

#include "processus.h"
#include "scheduler-priv.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h> /* pour initialiser le générateur aléatoire */

/*
 * Scheduler aléatoire: choisit au hasard parmi les prêts.
 * Ce scheduler est pseudo-équitable (aussi équitable que le générateur
 * pseudo-aléatoire est aléatoire).
 */

/* Note: il serait plus élégant (et presque plus simple) d'utiliser
 * une TSD pour stocker la structure "pret" de chaque processus, comme
 * cela est fait dans scheduler-prio. Je garde l'implantation bête pour
 * éviter toute confusion.
 */

struct pret {
    struct pret *suivant;       /* liste circulaire des prêts */
    struct pret *precedent;     /* liste circulaire des prêts */
    processus_t  qui;
};

/* Gestion de la file des processus prêts:
 * il s'agit d'une file circulaire doublement chaînée.
 * les_prets contient la tête de la file. */
static struct pret *les_prets = NULL;

/* Nombre de processus dans les files des prêts. */
static int nb_proc_prets = 0;

/****************************************************************/

static void sched_aleat_init (void)
{
    les_prets = NULL;
    nb_proc_prets = 0;
    srandom ((int) time(NULL));
}

/* Ajoute p à la fin de la file. */
static void sched_aleat_ajouter_pret (processus_t qui)
{
    struct pret *p = malloc (sizeof (struct pret));
    p->qui = qui;
    if (les_prets != NULL) { /* la file n'est pas vide */
        p->suivant = les_prets;
        p->precedent = les_prets->precedent;
        p->suivant->precedent = p;
        p->precedent->suivant = p;
    } else {                    /* la file était vide */
        les_prets = p;
        p->suivant = p;
        p->precedent = p;
    }
    nb_proc_prets++;
}

/* Extrait l'élément prêt p de la file spécifiée.
 * Il est préférable que p soit effectivement dans la file...
 */
static void extraire_processus (struct pret **la_file, struct pret *p)
{
    if (p->suivant != p) { /* la file ne devient pas vide */
        p->suivant->precedent = p->precedent;
        p->precedent->suivant = p->suivant;
        if (*la_file == p)      /* on extrait le premier */
          *la_file = p->suivant;
    } else {                    /* la file devient vide */
        *la_file = NULL;
    }
    p->suivant = NULL;          /* par sécurité */
    p->precedent = NULL;        /* id */
    free (p);
    nb_proc_prets--;
}

/* Extrait un processus au hasard dans la file des prêts. */
static processus_t sched_aleat_choisir_elu (void)
{
    /* XXXX A COMPLETER XXXX */
    processus_t elu;
    struct pret *info;
    int eluId = random()%nb_proc_prets;
    struct pret *iterator = les_prets;
    for (size_t i = 0; i < eluId; i++)
    {
        iterator = iterator->suivant;
    }
    elu = iterator->qui;
    extraire_processus(&les_prets,iterator);
    return elu;
    
}

/* Nombre de processus prêts. */
static int sched_aleat_nb_prets (void)
{
    return nb_proc_prets;
}

/* Pour debugger. */
static void sched_aleat_lister_prets (void)
{
    rendre_noninterruptible();
    printf ("SCHED: liste des prets:");
    if (les_prets == NULL) {
        printf (" <vide>");
    } else {
        struct pret *p;
        p = les_prets;
        do {
            printf (" %s/%p", proc_nom (p->qui), p->qui);
            p = p->suivant;
        } while (p != les_prets);
    }
    printf ("\n");
    rendre_interruptible();
}

/****************************************************************/

/* Pas de priorité => tout le monde à 1. */

static int sched_aleat_priorite_min(void)
{
    return 1;
}

static int sched_aleat_priorite_max(void)
{
    return 1;
}

static int  sched_aleat_get_priority (void)
{
    return 1;
}

static void sched_aleat_set_priority (int prio)
{
    /* nop */
}

static void sched_aleat_set_priority_other (processus_t qui, int prio)
{
    /* nop */
}

/****************************************************************/

static char *sched_aleat_nom (void)
{
    return "Aleatoire";
}

struct scheduler sched_aleatoire = {
    sched_aleat_nom,
    sched_aleat_init,
    sched_aleat_choisir_elu,
    sched_aleat_ajouter_pret,
    sched_aleat_nb_prets,
    sched_aleat_lister_prets,
    sched_aleat_priorite_min,
    sched_aleat_priorite_max,
    sched_aleat_get_priority,
    sched_aleat_set_priority,
    sched_aleat_set_priority_other
};

