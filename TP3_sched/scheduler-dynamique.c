/* Time-stamp: <23 mar 2018 11:39 queinnec@enseeiht.fr> */

#include "processus.h"
#include "scheduler-priv.h"
#include "TSD.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * Scheduler avec priorité absolue dynamique.
 *
 * Un processus de priorité i s'exécute i/j plus souvent qu'un
 * processus de priorité j.
 * Ce scheduler est équitable.
 */

/* Il est plus simple que PRIO_MIN soit 0 (indexage des tableaux).
 * Le niveau 0 est réservé (pour rebasculer à leur niveau max tous les
 * processus ensemble). */
#define PRIO_MIN 0
#define PRIO_MAX 10

/* Clef d'accès au TSD qui stocke les informations d'ordonnancement. */
static int clef_scheduler = -1;

/* Chaque processus possède une structure "pret", qui sert pour stocker
 * sa priorité et sa place dans une file des prêts.
 * (info->suivant == NULL) <=> pas dans une file de prêts.
 */
struct pret {
    int          prio_max;      /* priorité max du processs */
    int          prio_courante; /* priorité courante du processs */
    struct pret *suivant;       /* liste circulaire des prêts */
    struct pret *precedent;     /* liste circulaire des prêts */
    processus_t  qui;           /* pour retrouver le processus concerné */
};

/* Les listes (circulaires) des processus PRETs. */
/* Taille = PRIO_MAX+1 et non PRIO_MAX-PRIO_MIN+1 pour simplifier l'indexage
 * des tableaux. */
static struct pret *les_prets[PRIO_MAX + 1];

/* Nombre de processus dans les files des prêts. */
static int nb_proc_prets = 0;

/****************************************************************/

/* Initialisation du module. */
static void sched_prio2_init (void)
{
    int i;
    if (clef_scheduler == -1)
      clef_scheduler = TSD_creer_clef (free);
    assert (clef_scheduler != -1);
    for (i = PRIO_MIN; i <= PRIO_MAX; i++)
      les_prets[i] = NULL;
}

/****************************************************************/

static int sched_prio2_priorite_min(void)
{
    return PRIO_MIN+1;          /* ah ah */
}

static int sched_prio2_priorite_max(void)
{
    return PRIO_MAX;
}

/* Obtention de la donnée spécifique du processus `qui' qui contient
 * ses priorités et les pointeurs de chaînage.
 * Si la structure n'existe pas (première appel pour ce processus), elle
 * est créée et initialisée avec la priorité du processus créateur.
 */
static struct pret *get_info (processus_t qui)
{
    struct pret *info;
    info = TSD_get_other (qui, clef_scheduler);
    if (info == NULL) {         /* première fois => création */
        info = malloc (sizeof (struct pret));
        if (qui != proc_self()) { /* qui est le père? */
            struct pret *autreinfo;
            autreinfo = get_info (proc_self());
            info->prio_max = autreinfo->prio_max;
            info->prio_courante = info->prio_max;
        } else { /* seul le proc principal peut être dans cette situation. */
            info->prio_max = PRIO_MAX;
            info->prio_courante = PRIO_MAX;
        }
        info->qui = qui;
        info->suivant = NULL;
        info->precedent = NULL;
        TSD_set_other (qui, clef_scheduler, info);
    }
    return info;
}

/* Obtention de la priorité (maximale) du processus courant. */
static int sched_prio2_get_priority (void)
{
    struct pret *info;
    int priorite;
    rendre_noninterruptible();
    info = get_info (proc_self());
    priorite = info->prio_max;
    rendre_interruptible();
    return priorite;
}

/* Positionnement de la priorité (maximale) du processus courant. */
static void sched_prio2_set_priority (int prio)
{
    struct pret *info;
    if (prio < sched_prio2_priorite_min())
      prio = sched_prio2_priorite_min();
    if (prio > sched_prio2_priorite_max())
      prio = sched_prio2_priorite_max();
    rendre_noninterruptible();
    info = get_info (proc_self());
    if (sched_verbose)
      printf ("SCHED: %s: set priority %d -> %d\n", proc_nom(proc_self()), info->prio_max, prio);
    info->prio_max = prio;
    if (info->prio_courante > info->prio_max)
      info->prio_courante = info->prio_max;
    rendre_interruptible();
}

/****************************************************************/

/* Ajoute p à la fin de la file spécifiée. */
static void ajouter_pret (struct pret **la_file, struct pret *p)
{
    if (*la_file != NULL) { /* la file n'est pas vide */
        p->suivant = *la_file;
        p->precedent = (*la_file)->precedent;
        p->suivant->precedent = p;
        p->precedent->suivant = p;
    } else {                    /* la file était vide */
        *la_file = p;
        p->suivant = p;
        p->precedent = p;
    }
    nb_proc_prets++;
}

/* Extrait l'élément prêt p de la file spécifiée.
 * Il est nécessaire que p soit effectivement dans la file.
 */
static void extraire_pret (struct pret **la_file, struct pret *p)
{
    if (p->suivant != p) { /* la file ne devient pas vide */
        p->suivant->precedent = p->precedent;
        p->precedent->suivant = p->suivant;
        if (*la_file == p)      /* on extrait le premier */
          *la_file = p->suivant;
    } else {                    /* la file devient vide */
        *la_file = NULL;
    }
    p->suivant = NULL;
    p->precedent = NULL;
    nb_proc_prets--;
}

/* Ajoute p dans les prêts.
 * (selon la politique d'ordonnancement. Ici à la fin de la file
 * correspondant à la priorité courante de p). */
static void sched_prio2_ajouter_pret (processus_t qui)
{
    /* XXXX A COMPLETER XXXX */
    struct pret *info;
    info = get_info (qui);
    ajouter_pret(&(les_prets[info->prio_courante]), info);
}

/* Extrait le processus en tête des prêts.
 * (selon la politique d'ordonnancement). */
static processus_t sched_prio2_choisir_elu (void)
{
    /* XXXX A COMPLETER XXXX */
     int prio_actuelle = sched_prio2_priorite_max();
    struct pret *info;
    processus_t elu = NULL;
    while (les_prets[prio_actuelle] == NULL && prio_actuelle >= sched_prio2_priorite_min()){
        prio_actuelle = prio_actuelle - 1;
    }
    if (prio_actuelle >= sched_prio2_priorite_min()){
        // Il reste des processus à traiter
        elu = les_prets[prio_actuelle]->qui;
        info = get_info (elu);
        info->prio_courante = info->prio_courante - 1;
        extraire_pret(&(les_prets[prio_actuelle]), info);
        return elu;
    }else{
        // On remet les pret a leur prio max
        while (les_prets[PRIO_MIN] != NULL)
        {
            elu = les_prets[PRIO_MIN]->qui;
            info = get_info (elu);
            info->prio_courante = info->prio_max;
            extraire_pret(&(les_prets[PRIO_MIN]), info);
            sched_prio2_ajouter_pret(elu);
        }
        return sched_prio2_choisir_elu();
    }
    
}

/* Nombre de processus prêts. */
static int sched_prio2_nb_prets (void)
{
    return nb_proc_prets;
}

/****************************************************************/

/* Positionnement de la priorité (maximale) d'un autre processus. */
static void sched_prio2_set_priority_other (processus_t qui, int prio)
{
    struct pret *info;
    if (prio < sched_prio2_priorite_min())
      prio = sched_prio2_priorite_min();
    if (prio > sched_prio2_priorite_max())
      prio = sched_prio2_priorite_max();
    rendre_noninterruptible();
    info = get_info (qui);
    if (sched_verbose)
      printf ("SCHED: %s: set priority %d -> %d\n", proc_nom(qui), info->prio_max, prio);
    info->prio_max = prio;
    /* La priorité courante devient le min(ancienne priorité courante,
     *                                     nouvelle priorité max). */
    /* Le processus était dans une file de prêts, et sa priorité change. */
    if ((info->suivant != NULL) && (info->prio_courante > prio)) {
        extraire_pret (&(les_prets[info->prio_courante]), info);
        info->prio_courante = prio;
        ajouter_pret (&(les_prets[info->prio_courante]), info);
    }
    rendre_interruptible();
}


/****************************************************************/

/* Pour debugger. */
static void sched_prio2_lister_prets (void)
{
    int prio;
    struct pret *p;
    rendre_noninterruptible();
    printf ("SCHED: liste des prets:");
    for (prio = PRIO_MAX; prio >= PRIO_MIN; prio--) {
        printf (" [");
        if (les_prets[prio] != NULL) {
            p = les_prets[prio];
            do {
                printf (" %s/%p (max=%d)", proc_nom (p->qui), p->qui, p->prio_max);
                p = p->suivant;
            } while (p != les_prets[prio]);
        }
        printf (" ]");
    }    
    printf ("\n");
    rendre_interruptible();
}

/****************************************************************/

static char *sched_prio2_nom (void)
{
    return "Avec priorité dynamique";
}

struct scheduler sched_prio2_dynamique = {
    sched_prio2_nom,
    sched_prio2_init,
    sched_prio2_choisir_elu,
    sched_prio2_ajouter_pret,
    sched_prio2_nb_prets,
    sched_prio2_lister_prets,
    sched_prio2_priorite_min,
    sched_prio2_priorite_max,
    sched_prio2_get_priority,
    sched_prio2_set_priority,
    sched_prio2_set_priority_other
};
