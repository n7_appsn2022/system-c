/* Time-stamp: <08 avr 2020 14:10 queinnec@enseeiht.fr> */

#include "processus.h"
#include "scheduler-priv.h"
#include "TSD.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

/*
 * Scheduler basique: une seule file FIFO, sans priorité.
 * Ce scheduler est équitable.
 */

/* Clef d'accès au TSD qui stocke les informations d'ordonnancement. */
static int clef_scheduler = -1;

/* Chaque processus possède une structure "pret", qui sert pour stocker
 * sa place dans la file des prêts.
 * (info->suivant == NULL) <=> pas dans la file de prêts.
 */
struct pret {
    struct pret *suivant;       /* liste circulaire des prêts */
    struct pret *precedent;     /* liste circulaire des prêts */
    processus_t  qui;
};

/* Gestion de la file des processus prêts:
 * il s'agit d'une file circulaire doublement chaînée.
 * les_prets contient la tête de la file. */
static struct pret *les_prets = NULL;

/* Nombre de processus dans les files des prêts. */
static int nb_proc_prets = 0;

/****************************************************************/

/* Initialisation du module. */
static void sched_fifo_init (void)
{
    les_prets = NULL;
    if (clef_scheduler == -1)
      clef_scheduler = TSD_creer_clef (free);
    assert (clef_scheduler != -1);
    nb_proc_prets = 0;
}

/****************************************************************/

/* Obtention de la donnée spécifique du processus `qui' qui contient
 * ses priorités et les pointeurs de chaînage.
 * Si la structure n'existe pas (première appel pour ce processus), elle
 * est créée et initialisée avec la priorité du processus créateur.
 */
static struct pret *get_info (processus_t qui)
{
    struct pret *info;
    info = TSD_get_other (qui, clef_scheduler);
    if (info == NULL) {         /* première fois => création */
        info = malloc (sizeof (struct pret));
        info->qui = qui;
        info->suivant = NULL;
        info->precedent = NULL;
        TSD_set_other (qui, clef_scheduler, info);
    }
    return info;
}

/* Ajoute p à la fin de la file spécifiée. */
static void ajouter_pret (struct pret **la_file, struct pret *p)
{
    if (*la_file != NULL) { /* la file n'est pas vide */
        p->suivant = *la_file;
        p->precedent = (*la_file)->precedent;
        p->suivant->precedent = p;
        p->precedent->suivant = p;
    } else {                    /* la file était vide */
        *la_file = p;
        p->suivant = p;
        p->precedent = p;
    }
    nb_proc_prets++;
}

/* Extrait l'élément prêt p de la file spécifiée.
 * Il est nécessaire que p soit effectivement dans la file.
 */
static void extraire_pret (struct pret **la_file, struct pret *p)
{
    if (p->suivant != p) { /* la file ne devient pas vide */
        p->suivant->precedent = p->precedent;
        p->precedent->suivant = p->suivant;
        if (*la_file == p)      /* on extrait le premier */
          *la_file = p->suivant;
    } else {                    /* la file devient vide */
        *la_file = NULL;
    }
    p->suivant = NULL;
    p->precedent = NULL;
    nb_proc_prets--;
}

/****************************************************************/

/* Ajoute p à la fin de la file. */
static void sched_fifo_ajouter_pret (processus_t qui)
{
    struct pret *p;
    p = get_info (qui);
    assert (p->suivant == NULL); /* pas déjà dans la file */
    ajouter_pret (&les_prets, p);
}
/* Extrait le processus en tête de la file des prêts. */
static processus_t sched_fifo_choisir_elu (void)
{
    processus_t p;
    assert (les_prets != NULL);
    p = les_prets->qui;
    extraire_pret (&les_prets, les_prets);
    if (sched_verbose)
      printf ("SCHED: %s: selectionné\n", proc_nom(p));
    return p;
}

/* Nombre de processus prêts. */
static int sched_fifo_nb_prets (void)
{
    return nb_proc_prets;
}

/* Pour debugger. */
static void sched_fifo_lister_prets (void)
{
    rendre_noninterruptible();
    printf ("SCHED: liste des prets:");
    if (les_prets == NULL) {
        printf (" <vide>");
    } else {
        struct pret *p;
        p = les_prets;
        do {
            printf (" %s/%p", proc_nom (p->qui), p->qui);
            p = p->suivant;
        } while (p != les_prets);
    }
    printf ("\n");
    rendre_interruptible();
}

/****************************************************************/

/* Pas de priorité => tout le monde à 1. */

static int sched_fifo_priorite_min(void)
{
    return 1;
}

static int sched_fifo_priorite_max(void)
{
    return 1;
}

static int  sched_fifo_get_priority (void)
{
    return 1;
}

#pragma GCC diagnostic ignored "-Wunused-parameter"
static void sched_fifo_set_priority (int prio)
{
    /* nop */
}

#pragma GCC diagnostic ignored "-Wunused-parameter"
static void sched_fifo_set_priority_other (processus_t qui, int prio)
{
    /* nop */
}

/****************************************************************/

static char *sched_fifo_nom (void)
{
    return "FIFO";
}

struct scheduler sched_fifo = {
    sched_fifo_nom,
    sched_fifo_init,
    sched_fifo_choisir_elu,
    sched_fifo_ajouter_pret,
    sched_fifo_nb_prets,
    sched_fifo_lister_prets,
    sched_fifo_priorite_min,
    sched_fifo_priorite_max,
    sched_fifo_get_priority,
    sched_fifo_set_priority,
    sched_fifo_set_priority_other
};

