
/* Time-stamp: <18 jui 2010 10:33 queinnec@enseeiht.fr> */

/* Test de mémoire virtuelle, en utilisant la couche processus.
 * Deux pages, deux processus :
 *  P1 écrit un message dans chacune de ses pages,
 *  P2 prend la main et écrit un message dans chacune de ses pages (donc à la
 *     même adresse virtuelle que pour P1)
 *  P1 reprend la main et vérifie qu'il y a ses messages
 *  P2 reprend la main et vérifie qu'il y a ses messages
 */

#include "processus.h"
#include "memvirt.h"
#include "scheduler.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Pour éviter swap exhausted, il faut avoir
 *  NBPROC*NBPAGES_VIRT <= NBPAGES_SWAP.
 * Ici NBPROC = 2 (plus le pgm principal qui n'accède pas à la mémoire virtuelle. */
#define NBPAGES_VIRT 2
#define NBPAGES_SWAP 4

static int pagesize;
static char *base;

void code1 (void *unused)
{
    /* (2) */
    char *msg_page0 = "coucou, ceci est le message de P1 dans la page 0";
    char *msg_page1 = "ceci est le message de P1 dans la page 1";

    int ok = 1;

    strcpy(&(base[0]), msg_page0);
    strcpy(&(base[pagesize]), msg_page1);
    proc_commuter(); /* (3) P2 prend la main */
    /* (5) */
    if (strcmp(&(base[0]), msg_page0) != 0) {
        printf("P1 : on m'a écrasé la page 0 !\n");
        ok = 0;
    }
    if (strcmp(&(base[pagesize]), msg_page1) != 0) {
        printf("P1 : on m'a écrasé la page 1 !\n");
        ok = 0;
    }
    if (ok) {
        printf("P1 : ma mémoire virtuelle fonctionne correctement !\n");
    }
    proc_suspendre(); /* (6) P2 reprend la main */
}

void code2 (void *unused)
{
    /* (4) */
    char *msg_page0 = "ah ah ah ah";
    char *msg_page1 = "je tente d'écraser la page de P1 !";

    int ok = 1;

    strcpy(&(base[0]), msg_page0);
    strcpy(&(base[pagesize]), msg_page1);
    proc_commuter(); /* (5) P1 reprend la main */
    /* (6) */
    if (strcmp(&(base[0]), msg_page0) != 0) {
        printf("P2 : on m'a écrasé la page 0 !\n");
        ok = 0;
    }
    if (strcmp(&(base[pagesize]), msg_page1) != 0) {
        printf("P2 : on m'a écrasé la page 1 !\n");
        ok = 0;
    }
    if (ok) {
        printf("P2 : ma mémoire virtuelle fonctionne correctement !\n");
        exit (0); /* pouf */
    }
    /* NOTREACHED */
    proc_suspendre();
}

int main ()
{
    /* MV_verbose = 1;*/
    sched_set_scheduler (&sched_fifo); /* important */
    proc_init ();
    MV_init (NBPAGES_VIRT, NBPAGES_SWAP);
    base = (char*)MV_getbase();
    pagesize = MV_getpagesize();
    
    proc_activer ("P1", code1, NULL);
    proc_activer ("P2", code2, NULL);
    proc_suspendre (); /* (1) P1 prend la main */
    return 3;
}
