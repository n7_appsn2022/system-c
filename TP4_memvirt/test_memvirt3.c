
/* Time-stamp: <10 jui 2010 08:27 queinnec@enseeiht.fr> */

/* Test de mémoire virtuelle, en utilisant la couche sémaphore en préemptif.
   Il ne doit pas y avoir d'autres messages que les traces.
   Finit sur un interblocage. Si NBITER est suffisament petit, certains
   processus peuvent cependant parvenir à finir.
 */

#include "semaphores.h"
#include "preemption.h"          /* pour activer la préemption */
#include "pprocessus.h"          /* pour proc_nom (beurk) */
#include "sleep.h"
#include "memvirt.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>

/* Pour éviter swap exhausted, il faut avoir
   NBPROC*NBPAGES_VIRT <= NBPAGES_SWAP */
#define NBPAGES_VIRT 10
#define NBPAGES_SWAP 100
#define NBPROC 10
#define NBITER 5

/* Il vaut mieux avoir NBSEM << NBPROC, sinon l'interblocage est rapide. */
#define NBSEM 5
semaphore_t sem[NBSEM];

void code (void *arg)
{
    int moi = *(int *)arg;
    int i;
    int p;
    int pagesaccedees[NBITER];
    char *base = (char*)MV_getbase();
    int pagesize = MV_getpagesize();

    for (i = 0; i < NBITER; i++) {
        p = random() % NBPAGES_VIRT;
        pagesaccedees[i] = p;
        assert ((base[p*pagesize + p] == 0) || (base[p*pagesize + p] == moi));
        base[p*pagesize + p] = moi;
        sem_V (sem[random() % NBSEM]);
        if (random() % 2)
          proc_sleep (1);
        sem_P (sem[random() % NBSEM]);
    }
    /* probably NOTREACHED */
    printf ("**** Miracle, %s est passe!\n", proc_nom (proc_self()));
#if 1
    /* Avant de mourir vérifions que tout est bon. */
    for (i = 0; i < NBITER; i++) {
        p = pagesaccedees[i];
        if (base[p*pagesize + p] != moi) {
            printf ("**** PANIC: %s: page %d n'est pas a moi!\n",
                    proc_nom (proc_self()), p);
        }
    }
#endif
}

int main ()
{
    int i;
    char nom[50];
    /*sem_verbose = 1;*/
    /*preemption_verbose = 1;*/
    MV_verbose = 1;
    srandom (getpid());
    sem_init ();
    MV_init (NBPAGES_VIRT, NBPAGES_SWAP);
    for (i = 0; i < NBSEM; i++) {
        sprintf (nom, "SEM%d", i);
        sem[i] = sem_creer (nom, 0);
    }
    for (i = 1; i <= NBPROC; i++) {
        int *pi = malloc (sizeof(int));
        sprintf (nom, "PRO%d", i);
        *pi = i;
        sem_activer (nom, code, pi);
    }
    preemption_activate (1);
    sem_suspendre_PP ();
    return 3;
}
