
/* Time-stamp: <07 jui 2010 13:48 queinnec@enseeiht.fr> */

/* Attention, il existe un semaphore.h dans /usr/include. */

#ifndef _SEMAPHORES_H_
#define _SEMAPHORES_H_

typedef struct semaphore *semaphore_t;

extern int sem_verbose;

/* Création et activation d'un nouveau processus synchronisé par sémaphores. */
void sem_activer (char *nom, void (*code) (void *), void *arg);

/* Manipulation des sémaphores. */

/* Création d'un nouveau sémaphore, avec la valeur initiale spécifiée. */
semaphore_t sem_creer (char *nom, int val);
/* Obtention du nom du sémaphore. */
char *sem_nom (semaphore_t s);
/* Décrémente ou bloque le processus appelant si zéro. */
void sem_P (semaphore_t s);
/* Incrémente ou débloque un processus bloqué s'il y en a */
void sem_V (semaphore_t s);
/* Non-blocking P: return 0 if the lock is obtained, 1 if it would block. */
int sem_tryP (semaphore_t s);
/* Destruction du sémaphore. Il ne doit pas y avoir de processus bloqué. */
void sem_detruire (semaphore_t s);

/* sem_suspendre_PP permet de suspendre le programe principal.
 * sem_suspendre_PP ne revient jamais, et il n'y a aucun moyen de debloquer
 * le programme principal une fois suspendu. */
void sem_suspendre_PP (void);

/* Initialisation du module. */
void sem_init (void);

#endif
