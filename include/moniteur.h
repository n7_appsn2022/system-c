/* Time-stamp: <27 May 2003 09:37 queinnec@enseeiht.fr> */

/*
 * Implantation de la véritable sémantique des moniteurs.
 */

#ifndef _MONITEUR_H_
#define _MONITEUR_H_

#include <stdio.h>

extern int moniteur_verbose;

typedef struct moniteur *moniteur_t;
typedef struct varcond *varcond_t;

/* Création et activation d'un nouveau processus. */
void moniteur_activer (char *nom, void (*code) (void *), void *arg);

moniteur_t moniteur_creer (char *nom);
void       moniteur_entrer (moniteur_t);
void       moniteur_sortir (moniteur_t);

varcond_t VC_creer (moniteur_t m, char *nom);
char     *VC_nom (varcond_t vc);
void      VC_wait (varcond_t);
void      VC_signal (varcond_t);
/* La destruction de variables-condition est un peu surprenante, mais
 * pourquoi s'en priver ? */
void      VC_detruire (varcond_t);

/* Nombre de threads bloqués sur la variable condition spécifiée. */
int       VC_waiting (varcond_t);

/*
 * Affichage de l'état des files du moniteur et de ses variables conditions.
 */
void moniteur_debug (FILE *f, moniteur_t);

/* moniteur_suspendre_PP permet de suspendre le programe principal.
 * moniteur_suspendre_PP ne revient jamais, et il n'y a aucun moyen de
 * débloquer le programme principal une fois suspendu. */
void moniteur_suspendre_PP (void);

/* Initialisation du module. */
void moniteur_init (void);

#endif
