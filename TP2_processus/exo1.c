
/* ping/pong processus: utilisation de commuter */
   
#include "processus.h"
#include "scheduler.h"
#include <stdio.h>
#include <stdlib.h>

#define N 10
processus_t pmain, p1, p2;

static void ping_or_pong (void *texte)
{
    int i;
    for (i = 0; i < N; i++) {
        printf ("%s\n", (char *)texte);
        pproc_commuter();
    }
    printf ("%s a fini\n", (char *)texte);
}


int main ()
{
    //sched_set_scheduler (&sched_aleatoire); 
    sched_set_scheduler (&sched_prio_tempsreel);   
    /* sched_set_priority_other (p1, 3);
    sched_set_priority_other (p2, 6);
    sched_set_priority (1); */
    proc_init ();
    pmain = proc_self();
    p1 = proc_activer("Ping", ping_or_pong, "ping");
    p2 = proc_activer("Pong", ping_or_pong, "pong");
    printf ("Debut\n");
    proc_suspendre();
    printf ("Fin\n");
    return 0;
}
