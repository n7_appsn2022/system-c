
/* ping/pong processus avec TSD. */
   
#include "TSD.h"
#include "processus.h"
#include "scheduler.h"
#include <stdio.h>
#include <string.h>

#define N 10
processus_t pmain, pping,ppong;

/* Ne pas changer l'interface de la procédure boucle.
 * Utiliser une donnée spécifique (TSD) pour obtenir le texte à afficher. */
static void boucle (void)
{
    int i;
    for (i = 0; i < N; i++) {
        printf ("%s\n", (char*)TSD_get(1));
        proc_commuter();
    }
    printf ("%s done\n", (char*)TSD_get(1));
}

void tsd_init(void *argument){  
    TSD_set(1, argument);   
    boucle(); 
    if(strcmp((char*)TSD_get,"pong") == 0){
        proc_continuer(pmain);
    }
}

int main ()
{
    proc_init ();
    pmain = proc_self();
    pping = proc_activer("Ping", tsd_init, "ping");
    ppong = proc_activer("Pong", tsd_init, "pong");
    printf ("Debut\n");
    proc_suspendre();
    printf ("Fin\n");
    return 0;
}
