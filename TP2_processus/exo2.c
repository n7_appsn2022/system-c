
/* ping/pong processus: utilisation de continuer/suspendre (exclusivement). */
   
#include "processus.h"
#include "scheduler.h"
#include <stdio.h>

#define N 10
processus_t pmain, pping,ppong;

static void pong (void *unused)
{
    int i;
    for (i = 0; i < N; i++) {
        printf ("%s\n", "Pong");
        proc_continuer(pping);
        proc_suspendre();
    }
    printf ("%s a fini\n", "Pong");
    proc_continuer(pmain);
}

static void ping (void *unused)
{
    ppong = proc_activer("Pong", pong, NULL);
    int i;
    for (i = 0; i < N; i++) {
        printf ("%s\n", "Ping");
        proc_suspendre();
        proc_continuer(ppong);
    }
    printf ("%s a fini\n", "Ping");
}


int main ()
{
    sched_set_scheduler (&sched_aleatoire); /* ça devrait marcher avec */
    proc_init ();
    pmain = proc_self();
    pping = proc_activer("Ping", ping, NULL);
    printf ("Debut\n");
    proc_suspendre();
    printf ("Fin\n");
    return 0;
}
